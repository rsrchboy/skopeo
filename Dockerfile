#      _
#  ___| | _____  _ __   ___  ___
# / __| |/ / _ \| '_ \ / _ \/ _ \
# \__ \   < (_) | |_) |  __/ (_) |
# |___/_|\_\___/| .__/ \___|\___/
#               |_|

FROM golang:alpine as build

RUN apk add --update git bash wget

RUN mkdir /locals

RUN go get -v github.com/genuinetools/reg

RUN apk add --update build-base linux-headers gpgme-dev lvm2-dev go-md2man
RUN git clone --depth=1 https://github.com/containers/skopeo.git \
    && cd skopeo \
    && make bin/skopeo \
    && mkdir -p /skopeo \
    && make install DESTDIR=/skopeo

RUN mv /go/bin/* /locals

FROM alpine:latest

COPY --from=build --chown=0:0 /locals/* /usr/local/bin/
COPY --from=build --chown=0:0 /skopeo/  /

RUN apk add --no-cache gpgme device-mapper-libs git ca-certificates bash
